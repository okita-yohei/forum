package com.example.demo.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class CommentController {
	@Autowired
	CommentService commentService;
	@Autowired
	ReportService reportService;

	// コメント処理
	@PostMapping("{reportId}/add")
	public ModelAndView addText(@ModelAttribute("formModel") Comment comment, @PathVariable Integer reportId) {
		comment.setReportId(reportId);
		// コメントをテーブルに格納
		commentService.saveComment(comment);
		// コメントが更新されたら紐づく投稿のupdatedDateも更新
		Report report = reportService.editReport(reportId);
		Date date = new Date();
		report.setUpdatedDate(date);
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント編集画面
	@GetMapping("{reportId}/edit/{commentId}")
	public ModelAndView editContent(@PathVariable Integer reportId, @PathVariable Integer commentId ) {
		ModelAndView mav = new ModelAndView();
		// 編集するコメントを取得
		Comment comment = commentService.editComment(commentId);
		mav.addObject("reportId", reportId);
		// 編集するコメントをセット
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/editComment");
		return mav;
	}

	// コメント編集処理
	@PutMapping("{reportId}/update/{commentId}")
	public ModelAndView updateComment(@PathVariable Integer reportId, @PathVariable Integer commentId, @ModelAttribute("formModel") Comment comment) {
		// UrlParameterのidを更新するentityにセット
		comment.setId(commentId);
		comment.setReportId(reportId);
		Comment editComment = commentService.editComment(commentId);
		comment.setCreatedDate(editComment.getCreatedDate());
		// 編集した投稿を更新
		// commentのsetIdをコントローラー内で行うように試してみる
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}
}
