package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	@GetMapping
	public ModelAndView top(@RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate) throws ParseException {

		ModelAndView mav = new ModelAndView();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDateTime = dateFormat.parse("2021-04-01 00:00:00");
		Date endDateTime = new Date();

		if(!StringUtils.isBlank(startDate)) {
			startDateTime = dateFormat.parse(startDate + " 00:00:00");
		}

		if(!StringUtils.isBlank(endDate)) {
			endDateTime = dateFormat.parse(endDate + " 23:59:59");
		}

		// 投稿を全権取得
		List<Report> contentData = reportService.findAllReport(startDateTime, endDateTime);

		// コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定（）
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// コメントデータオブジェクトを保管（htmlで呼び出せるようにする）
		mav.addObject("comments", commentData);
		return mav;
	}

	//新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Report report = reportService.editReport(id);
		// 編集する投稿をセット
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		Report editReport = reportService.editReport(id);
		report.setCreatedDate(editReport.getCreatedDate());
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}
