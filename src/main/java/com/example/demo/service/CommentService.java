package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	// コメント全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAllByOrderByUpdatedDateDesc();
	}

	// コメント追加
	public void saveComment(Comment comment) {
		commentRepository.saveAndFlush(comment);
	}

	// コメント1件取得
	public Comment editComment(Integer commentId) {
		Comment comment = (Comment) commentRepository.findById(commentId).orElse(null);
		return comment;
	}

	// コメント削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
